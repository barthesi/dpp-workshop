---
site: blogdown:::blogdown_site
---

The workshop will take place in Grenoble, April 15th to 17th, starting at 10.30 AM on the 15th and ending at 12.30 AM on the 17th. 

### Topic

The topic for the workshop is Determinantal Point Processes, and more specifically computational methods, recent theoretical developments, and current challenges. 

### Speakers

Here is a preliminary list of speakers: 

- Rémi Bardenet (CNRS, CRIStAL, Lille)
- Ayoub Belhadji  (CRIStAL, Lille)
- Christophe Biscio (Aalborg)
- Guillaume Gautier (CRIStAL, Lille)
- Adrien Hardy (Univ. de Lille)
- Adrien Kassel (CNRS, ENS Lyon)
- Claire Launay (Univ. Paris Descartes)
- Mylène Maida (Univ. de Lille)
- Adrien Mazoyer (Univ. de Lille)
- Arnaud Poinas (Univ. de Lille)

### Schedule

TBA

### Organisers 

Pierre-Olivier Amblard, Nicolas Tremblay, Simon Barthelmé (Gipsa-lab, Grenoble). Contact us at *dppworkshop at gipsa-lab dot fr*. 

### Venue

[IMAG](https://batiment.imag.fr/), Grenoble, in room 106 (Wed. and Fri.) and
room 206 (Thurs.). 
If you're coming from Grenoble main station, take tram B and get off at Gabriel
Fauré. 
Please see [here](https://batiment.imag.fr/en/contact-addresses-access) for detailed directions. 

# Registration 

Registration is free but mandatory. Register by filling out
the online form [here](https://framaforms.org/dpp-in-the-alps-1579619724). 
