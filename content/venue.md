---
title: "Venue"
date: 2020-02-04
draft: true
---

[IMAG](https://batiment.imag.fr/), Grenoble, in room 106 (Wed. and Fri.) and
room 206 (Thurs.). 
If you're coming from Grenoble main station, take tram B and get off at Gabriel
Fauré. 
Please see [here](https://batiment.imag.fr/en/contact-addresses-access) for detailed directions. 
