---
title: "Sponsors"
date: "2020-02-04"
---

The workshop is supported by the [Grenoble Data
Institute](https://data-institute.univ-grenoble-alpes.fr/) and by the ANR
(project
[GenGP](https://barthesi.gricad-pages.univ-grenoble-alpes.fr/personal-website/gengp/)).

![](../logo_data_inst.png) 
![](https://anr.fr/typo3conf/ext/anr_skin/Resources/Public/assets/img/anr-logo.png)

