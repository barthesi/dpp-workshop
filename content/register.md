---
title: "Register"
date: 2020-02-04
draft: true
---

Registration is free but mandatory (we have a limited number of seats). Register by filling out
the online form [here](https://framaforms.org/dpp-in-the-alps-1579619724). 
