---
title: "Simon Barthelmé"
date: "2018-11-25T21:48:51-07:00"
---

I'm a [CNRS](http://cnrs.fr/en) Research Fellow. I work at [GIPSA-lab](http://www.gipsa-lab.org), in the signal processing department. I am part of the VIBS team, which focuses on brain signals. 

I do research in computational statistics, mostly theoretical but with applications to chemical sensors. My previous field was computational neuroscience. 

Contact me at name.surname@gipsa-lab.fr.
