---
title: "Speakers"
date: "2020-02-04"
---


- Rémi Bardenet (CNRS, CRIStAL, Lille): Determinantal point processes are everywhere: tractable repulsiveness for
statistical models and computational tools
- Ayoub Belhadji  (CRIStAL, Lille): Kernel quadrature and interpolation using determinantal sampling
- Christophe Biscio (Aalborg): Determinantal point processes in spatial statistics: modelisation, mixing and simulation.
- Victor-Emmanuel Brunel (ENSAE, CREST, Paris): Challenges in learning discrete DPPs
- Guillaume Gautier (CRIStAL, Lille): TBA
- Adrien Hardy (Univ. de Lille): TBA
- Adrien Kassel (CNRS, ENS Lyon): DLPs in the ALPs
- Claire Launay (Univ. Paris Descartes): Determinantal point processes and image processing: some applications.
- Mylène Maida (Univ. de Lille): TBA
- Adrien Mazoyer (Univ. de Lille): Monte-Carlo integration of non-differentiable functions using determinantal point processes.
- Arnaud Poinas (Univ. de Lille): TBA
- Konstantin Usevich (CNRS, CRAN, Nancy): DPPs in the flat limit
