---
title: "Organisers"
date: 2020-02-04
---

The workshop is organised by Pierre-Olivier Amblard, Nicolas Tremblay and Simon Barthelmé (Gipsa-lab, Grenoble). Contact us at *dppworkshop at gipsa-lab dot fr*. 
